## [6.7.2] - 2020-08-19
### Library
- Packaged with `lib 6.7.15`

## [6.7.1] - 2020-07-24
### Fixed
- Increased monitor threshold to prevent fake buffers for tvs reporting integer playheads

## [6.7.0] - 2020-07-20
### Library
- Packaged with `lib 6.7.12`

## [6.5.0] - 2018-10-10
### Added
- Changed seek detection
### Library
- Packaged with `lib 6.5.16`

## [6.3.0] - 2018-06-19
### Library
- Packaged with `lib 6.3.0`
