var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.VieraHtml5 = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.player.currentTime
  },

  /** Override to return video duration */
  getDuration: function () {
    return this.player.duration
  },

  /** Override to return resource URL. */
  getResource: function () {
    return this.player.currentSrc
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'Panasonic Viera'
  },

  registerListeners: function () {
    // Some devices report the playhead as an integer, not as a float
    // Keep always this value higher than 1000
    this.monitorPlayhead(true, true, 1100)

    this.references = {
      play: this.playListener.bind(this),
      timeupdate: this.timeupdateListener.bind(this),
      pause: this.pauseListener.bind(this),
      playing: this.playingListener.bind(this),
      error: this.errorListener.bind(this),
      seeking: this.seekingListener.bind(this),
      seeked: this.seekedListener.bind(this),
      ended: this.endedListener.bind(this)
    }
    for (var key in this.references) {
      this.player.addEventListener(key, this.references[key])
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
        delete this.references[key]
      }
    }
  },

  /** Listener for 'play' event. */
  playListener: function (e) {
    this.fireStart()
  },

  /** Listener for 'timeupdate' event. */
  timeupdateListener: function (e) {
    if (this.getPlayhead() > 0.1) {
      this.fireStart()
      this.fireJoin()
    }
  },

  /** Listener for 'pause' event. */
  pauseListener: function (e) {
    this.firePause()
  },

  /** Listener for 'playing' event. */
  playingListener: function (e) {
    this.fireResume()
    this.fireBufferEnd()
    this.fireJoin()
  },

  /** Listener for 'error' event. */
  errorListener: function (e) {
    this.fireError()
  },

  /** Listener for 'seeking' event. */
  seekingListener: function (e) {
    this.fireSeekBegin()
  },

  /** Listener for 'seeked' event. */
  seekedListener: function (e) {
    this.fireSeekEnd()
    if (this.monitor) this.monitor.skipNextTick()
  },

  /** Listener for 'ended' event. */
  endedListener: function (e) {
    this.fireStop()
  }
})

module.exports = youbora.adapters.VieraHtml5
